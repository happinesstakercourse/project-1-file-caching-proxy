// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>
// 15-640 Distributed System Project 2 - Cache File System


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;


public class MyHash {

    static public final int HASH_VALID = 1;


    /**
     * Get the md5 hash of a file, computed by file content
     *
     * @param fileName specified filename
     * @return byte array of the md5 digest
     * @throws IOException may throw IO exception
     */
    static public byte[] getHash(String fileName) throws IOException{

        byte[] data = new byte[(int)Proxy.CHUNK_SIZE];
        byte[] digest;
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            // read the file chunk by chunk because of limited memory
            synchronized (Proxy.globalFileLock) {
                RandomAccessFile raFd = new RandomAccessFile(fileName, "r");
                while (raFd.read(data) != -1) {
                    md.update(data);
                }
            }
            digest = md.digest();
        } catch (NoSuchAlgorithmException eNoAlgo) {
            System.out.println("MD5 Not Available");
            return null;
        } catch (FileNotFoundException eFileNotFound) {
            return null;
        }

        return digest;
    }


    /**
     * Verify whether specified file has the same hash as provided digest
     *
     * @param fileName file to compare with
     * @param digest provided digest as reference
     * @return true for the same
     */
    static public boolean verifyHash(String fileName, byte[] digest) {

        byte[] thisDigest;
        try {
            thisDigest = getHash(fileName);
        } catch (IOException eIO) {
            return false;
        }

        return Arrays.equals(digest, thisDigest);
    }


    // helper function to convert md5 byte array to printable string format
    static public String md52Str(byte[] digest) {
        StringBuffer hexStr = new StringBuffer();
        for (byte b: digest) {
            hexStr.append(Integer.toHexString((int)(b & 0xff)));
        }
        return hexStr.toString();
    }
}
