/**
 * This helper class serves to pass information from server to proxy
 * when pulling files.
 */

import java.io.Serializable;

public class GetFileStatus implements Serializable {
    public int status = 0;             // get file status code
    public byte[] buffer = null;       // data buffer returned
    public long length = 0;            // total length of file (useful in large file)
    public boolean largeFile = false;  // whether requested file is large file
}
