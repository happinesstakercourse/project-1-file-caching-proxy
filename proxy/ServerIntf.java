import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerIntf extends Remote {

    /**
     * Interface to get file from server
     */
    GetFileStatus getFile(String fileName, byte[] digest) throws RemoteException;

    /**
     * Interface to get large file from server
     */
    byte[] getLargeFile(String fileName, long offset, boolean lastRead) throws RemoteException;

    /**
     * Interface to post file to server
     */
    int postFile(String fileName, byte[] buffer) throws RemoteException;

    /**
     * Interface to post large file to server
     */
    int postLargeFile(String fileName, byte[] buffer, long offset,
                             boolean firstWrite, boolean lastWrite) throws RemoteException;

    /**
     * Interface to unlink server master file
     */
    int unlinkFile(String fileName) throws RemoteException;

}