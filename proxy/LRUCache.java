// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>
// 15-640 Distributed System Project 2 - Cache File System

// Hash Implementation for LRU cache to reduce search time to O(1)

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

public class LRUCache {

    public static final int EXIST_FILE = -1;

    /**
     * FileNode Class
     *
     * Helper class to represent each node in cache
     */
    private class FileNode {
        FileNode prev;  // prev node in linked list
        FileNode next;  // next node in linked list
        String key;     // filename as key
        int value;      // this field stores reference of this file

        private FileNode (String key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    private long capacity;
    private volatile long usage;
    private HashMap<String, FileNode> cacheMap = new HashMap<>();

    // tail is Most Recent Accessed File
    private FileNode head = new FileNode("", -1);
    private FileNode tail = new FileNode("", -1);

    public LRUCache(long capacity) {
        this.capacity = capacity;
        this.usage = 0;
        head.next = tail;
        tail.prev = head;
    }


    /**
     * The function would adjust the total usage of the LRU cache.
     * This would be called when only space need to be changed without modifying
     *     file access history
     *
     * @param delta change of space required
     * @return status code
     */
    public int adjustUsage(long delta) {

        if (delta <= 0) {
            // shrink usage, easy
            usage += delta;
            return 0;
        } else {
            // increase usage, may need eviction
            int rv = evictFile(delta);
            if (rv < 0) {
                return rv;
            }
            usage += delta;
            return 0;
        }
    }


    /**
     * The main access point to LRU cache. Access specified file and update
     * cache history for it.
     *
     * If space is not enough for the access, evict least recently used node
     * which is not opened to save space.
     *
     * @param key filename of the access
     * @param requestSpace optional argument
     *                     Positive Num: Allocate new space as specified
     *                     EXIST_FILE: current file exist on disk, read length dynamically
     * @return status code
     */
    public int accessFile(String key, long requestSpace) {

        int rv;

        // file already in cache list, move to tail
        if (cacheMap.containsKey(key)) {
            FileNode ptr = cacheMap.get(key);
            ptr.prev.next = ptr.next;
            ptr.next.prev = ptr.prev;
            moveTail(ptr);
        }

        // file not in list, insert it, if necessary evict LRU node
        else {
            long curLength;
            if (requestSpace != EXIST_FILE) {
                curLength = requestSpace;
            } else {
                File curFile = new File(key);
                curLength = curFile.length();
            }

            // try evict files to leave space for new cache file
            rv = evictFile(curLength);

            // not enough space, report error
            if (rv < 0) {
                return rv;
            }

            // insert the node
            FileNode ptr = new FileNode(key, 0);
            usage += curLength;
            moveTail(ptr);
            cacheMap.put(key, ptr);
        }

        return 0;
    }


    /**
     * Procedure to search the victim node to evict
     *
     * @param requiredSpace required space to be newly allocated
     * @return status code
     */
    private int evictFile(long requiredSpace) {

        while (usage + requiredSpace > capacity) {
            FileNode victim = head.next;

            // skip currently opened files
            while (checkOpened(victim)) {
                if (victim.value == -1) {
                    // reach end, no space for eviction
                    return FileHandling.Errors.EMFILE;
                }
                victim = victim.next;
            }

            // actually evict victim
            deleteNode(victim.key, true);
        }
        return 0;
    }


    /**
     * Function execute real work to delete the node from LRU
     *
     * @param key FileName as a key
     * @param deleteFile whether file on disk should be deleted
     */
    public void deleteNode(String key, boolean deleteFile) {
        if (!cacheMap.containsKey(key)) {
            return;
        }

        FileNode victim = cacheMap.get(key);
        File evictFile = new File(key);
        long evictLength = evictFile.length();

        // unlink from linked list
        victim.prev.next = victim.next;
        victim.next.prev = victim.prev;

        // delete from map
        cacheMap.remove(victim.key);

        // adjust usage
        usage -= evictLength;

        // delete actual file if asked to do so
        if (deleteFile) {
            try {
                File unlinkFile = new File(key);
                Files.delete(unlinkFile.toPath());
            } catch (IOException eIO) {
                // ignore IO error
                return;
            }
        }

        System.out.println("File " + key + " evicted! Usage" + usage);
    }


    // helper function to check whether given node is opened by readers
    private boolean checkOpened(FileNode node) {
        if (node.key.equals("")) {
            return true;
        }

        return node.value > 0;
    }


    // helper function to increase/decrease reference of node
    public int changeReference(String key, int delta) {
        if (!cacheMap.containsKey(key)) {
            return -1;
        }

        // only allow increase/decrease by 1
        if (delta != 1 && delta != -1) {
            return -1;
        }

        int prevRef = cacheMap.get(key).value;
        prevRef += delta;

        // just in case change to negative, ideally would not happen
        if (prevRef < 0) {
            prevRef = 0;
        }

        cacheMap.get(key).value = prevRef;

        return 0;
    }


    // helper function to get the reference count of a node
    public int getReference(String key) {
        if (!cacheMap.containsKey(key)) {
            return -1;
        }
        return cacheMap.get(key).value;
    }


    // helper function to move a node to tail (most recently used)
    private void moveTail(FileNode ptr) {
        ptr.prev = tail.prev;
        tail.prev = ptr;
        ptr.prev.next = ptr;
        ptr.next = tail;

        System.out.println("File " + ptr.key + " Accessed! Usage:" + usage);
    }

}
