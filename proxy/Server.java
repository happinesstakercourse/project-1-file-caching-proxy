// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>
// 15-640 Distributed System Project 2 - Cache File System


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Server extends UnicastRemoteObject implements ServerIntf{

    private static final int EIO = -5;
    private static final Lock globalFileLock = new ReentrantLock(true);

    private String rootDir = "/";

    private Server(String root) throws RemoteException{
        rootDir = root + "/";
    }


    /**
     * GetFile performs RPC from proxy to server, attempting to read a file given
     * filename on server, and return the latest content to proxy for caching.
     *
     * @param fileName plain filename specified by proxy
     * @return GetFileStatus class containing returned information
     * @throws RemoteException remote call exception may occur
     */
    public GetFileStatus getFile(String fileName, byte[] digest) throws RemoteException {
        String dir = rootDir + fileName;
        GetFileStatus ret = new GetFileStatus();

        if (!checkFileRange(fileName)) {
            ret.status = FileHandling.Errors.EPERM;
            return ret;
        }

        // check whether file exist
        File readFile = new File(dir);
        if (!readFile.exists()) {
            ret.status = FileHandling.Errors.ENOENT;
            return ret;
        }

        // if requested file is directory, tell proxy this is directory, let it handle
        if (readFile.isDirectory()) {
            ret.status = FileHandling.Errors.EISDIR;
            return ret;
        }

        // verify cache validity, if identical then tell proxy no need to update
        if (MyHash.verifyHash(dir, digest)) {
            ret.status = MyHash.HASH_VALID;
            return ret;
        }

        // read file into buffer
        globalFileLock.lock();

        // turn into random access file
        RandomAccessFile raFd;
        long fileSize;
        try {
            raFd = new RandomAccessFile(readFile, "r");
            fileSize = raFd.length();
        } catch (FileNotFoundException eNoFile) {
            // impossible
            ret.status = FileHandling.Errors.ENOENT;
            return ret;
        } catch (SecurityException eSec) {
            ret.status = FileHandling.Errors.EPERM;
            return ret;
        } catch (IOException eIO) {
            ret.status = EIO;
            return ret;
        }

        ret.length = fileSize;

        // large file mode, do not release lock until the large file transmitted
        if (fileSize > Proxy.CHUNK_SIZE) {
            ret.buffer = null;
            ret.largeFile = true;
            ret.status = 0;
            return ret;
        }

        try {
            ret.buffer = new byte[(int) fileSize];
            raFd.read(ret.buffer);
            raFd.close();
        } catch (IOException eIO) {
            ret.buffer = null;
            ret.status = EIO;
            return ret;
        } finally {
            globalFileLock.unlock();
        }

        ret.status = 0;
        return ret;
    }


    /**
     * postFile executes RPC from proxy to server to write modification to
     * master copy on server.
     *
     * @param fileName string of filename to write to
     * @param buffer file content to write
     * @return write status code
     * @throws RemoteException remote call exception may occur
     */
    public int postFile(String fileName, byte[] buffer) throws RemoteException{

        String dir = rootDir + fileName;
        File writeFile = new File(dir);

        RandomAccessFile raFd;
        try {
            raFd = new RandomAccessFile(writeFile, "rw");
        } catch (FileNotFoundException eNoFile) {
            // no disk space
            return EIO;
        }

        globalFileLock.lock();
        try {
            raFd.setLength(0);
            raFd.write(buffer);
            raFd.close();
        } catch (IOException eIO) {
            return EIO;
        } finally {
            globalFileLock.unlock();
        }

        return 0;
    }


    /**
     * Special handler for get large files, would read in loop from server file and write
     * chunk by chunk into buffer and return to proxy.
     *
     * @param fileName plain filename from proxy
     * @param offset file pointer offset to start reading
     * @param lastRead whether last chunk
     * @return returned data
     * @throws RemoteException
     */
    public byte[] getLargeFile(String fileName, long offset, boolean lastRead) throws RemoteException{

        byte[] ret;
        long fileSize;
        RandomAccessFile raFd;

        try {
            raFd = new RandomAccessFile(rootDir + fileName, "r");
            fileSize = raFd.length();
        } catch (FileNotFoundException eNotFound) {
            // not possible
            return null;
        } catch (IOException eIO) {
            return null;
        }

        // last read would compute buffer length to save space
        if (lastRead) {
            ret = new byte[(int)(fileSize - offset)];
        } else {
            ret = new byte[(int)Proxy.CHUNK_SIZE];
        }

        try {
            raFd.seek(offset);
            raFd.read(ret);
        } catch (IOException eIO) {
            return null;
        }

        // last read would release the file lock
        if (lastRead) {
            try {
                globalFileLock.unlock();
            } catch (IllegalMonitorStateException eMon) {
                // caused by threading issues
                return ret;
            }
        }

        return ret;
    }


    /**
     * Special handler for post large file, would read chunk by chunk from
     * proxy and write into server file.
     *
     * @param fileName plain filename provided by proxy
     * @param buffer data buffer to write
     * @param offset file pointer offset to start writing
     * @param firstWrite whether first chunk
     * @param lastWrite whether last chunk
     * @return status code
     * @throws RemoteException
     */
    public int postLargeFile(String fileName, byte[] buffer, long offset,
                             boolean firstWrite, boolean lastWrite) throws RemoteException{

        RandomAccessFile raFd;

        try {
            raFd = new RandomAccessFile(rootDir + fileName, "rw");
            if (firstWrite) {
                globalFileLock.lock();
                raFd.setLength(0);
            }
            raFd.seek(offset);
            raFd.write(buffer);
            raFd.close();

            if (lastWrite) {
                globalFileLock.unlock();
            }
        } catch (IOException eIO) {
            return EIO;
        }

        return 0;
    }


    /**
     * unlinkFile would delete the master copy on server side as requested by proxy.
     *
     * @param fileName file to unlink
     * @return status code
     * @throws RemoteException remote call exception may occur
     */
    public int unlinkFile(String fileName) throws RemoteException {

        File unlinkFile = new File(rootDir + fileName);
        if (!unlinkFile.exists()) {
            return FileHandling.Errors.ENOENT;
        }
        if (unlinkFile.isDirectory()) {
            return FileHandling.Errors.EISDIR;
        }

        synchronized (globalFileLock) {
            try {
                Files.delete(unlinkFile.toPath());
            } catch (IOException eIO) {
                return EIO;
            }
        }

        return 0;
    }


    // check whether request file is under server root directory
    private boolean checkFileRange(String fileName) {
        String fullFileName = Paths.get(rootDir + fileName).normalize().toString();
        String fullRootName = Paths.get(rootDir).normalize().toString();

        return fullFileName.startsWith(fullRootName);
    }


    public static void main(String args[]) {

        if (args.length < 2) {
            System.out.println("Usage: Server [port] [root dir]");
            return;
        }

        try {
            Server server = new Server(args[1]);
            Registry reg = LocateRegistry.createRegistry(Integer.parseInt(args[0]));
            reg.rebind("CacheServer", server);
        } catch (Exception eGeneral) {
            System.out.println("Server Initializing Error: " + eGeneral.getMessage());
            eGeneral.printStackTrace();
        }

    }

}
