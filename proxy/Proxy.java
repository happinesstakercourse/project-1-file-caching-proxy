// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>
// 15-640 Distributed System Project 2 - Cache File System


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

class Proxy {

    // constants
    public static final long CHUNK_SIZE = 1024 * 600; // Chunk size 600 KB

    // properties set by argument
    private static ServerIntf server;
    private static String cacheDir;
    private static LRUCache proxyCache;

    // locks
    private static final Object cacheLock = new Object();
    public static final Object globalFileLock = new Object();

    // master file pointer, only touched when open/close
    public static HashMap<String, String> masterDict = new HashMap<>();


    /**
     * FileHandler Class
     *
     * Factory Class which would be spawned to serve each client once.
     * Public methods in this class is the transparent interface to clients
     *     like normal C file operations, include:
     *
     *     open
     *     close
     *     read
     *     write
     *     unlink
     *     lseek
     *
     */
	private static class FileHandler implements FileHandling {

	    private static final int EIO = -5;

	    // Per client fd map, which saves information for each fd opened
        // for client
	    private HashMap<Integer, FileInfo> openedFdMap = new HashMap<>();
	    private int fdStart = 3;

        /**
         * FileInfo Class
         *
         * Helper private class to store information for internal management
         * usage for fd mapping.
         */
        private class FileInfo {
            private RandomAccessFile raFd; // opened file accessor for fd
            private boolean writePerm;     // whether write permission required
            private boolean isDir;         // whether opened file is directory
            private String fileName;       // file name on at cache side
            private String rawName;        // raw name passed in at open, may contain dir
            private String tmpName;        // tmp file name assigned to fd
        }

        /**
         * Open interface from proxy to client, convert from open option to
         * random access file open option.
         *
         * Fd is incrementally selected to client.
         *
         * Calling Structure:
         *   open
         *   |-> openFileSafe
         *       |-> updateCacheFile
         *       |-> openTmpFile
         *           |-> createTmpFile
         *
         * The high level procedure in open file is:
         * 1. check local master copy of cache, calculate hash
         * 2. upload to server and check validity
         * 3. update local cache if necessary
         * 4. if read request, give out current master copy to client
         * 5. if write request, create tmp copy and return to client
         *
         * This maintains the open-close session semantics of cache, and uses
         * check-on-use cache invalidation scheme.
         *
         * @param name Filename provided by client for file to access
         * @param o Open file option
         * @return Assigned fd to client
         */
		public int open(String name, OpenOption o) {

	        int myFd;
	        FileInfo myInfo = new FileInfo();

	        // store raw file name which may contain subdir prefix
            myInfo.rawName = name;
	        Path path = Paths.get(name);
	        String fileName = path.getFileName().toString();

            // set our self-defined options according to open option
            switch(o) {
                case READ:
                    myFd = openFileSafe(fileName, "r", true, false, myInfo);
                    break;
                case WRITE:
                    myFd = openFileSafe(fileName, "rw", true, false, myInfo);
                    break;
                case CREATE:
                    myFd = openFileSafe(fileName, "rw", false, false, myInfo);
                    break;
                case CREATE_NEW:
                    myFd = openFileSafe(fileName, "rw", false, true, myInfo);
                    break;
                default:
                    myFd = Errors.EINVAL;
            }

            // return errno on failure
            if (myFd < 0) {
                return myFd;
            }

            // select fd and attach to open fd map
            myFd = fdStart++;
            openedFdMap.put(myFd, myInfo);

            return myFd;
		}


        /**
         * Write interface would write to the tmp file allocated for client for
         * particular fd.
         * At the time of write, only that client can see the change.
         * Change only merged at close.
         *
         * @param fd file descriptor provided by client
         * @param buf buffer to read from client and write to file
         * @return byte written, assume the same as buffer length
         */
		public long write(int fd, byte[] buf) {

            if (checkDir(fd)) {
                return Errors.EISDIR;
            }

		    RandomAccessFile raFd = getFdFromMap(fd, true);
		    if (raFd == null) {
		        return Errors.EBADF;
            }

            // compute the increase of length ahead
            // tmp write file is not shared, no need for lock
            long delta;
            try {
                long curPos = raFd.getFilePointer();
                long afterLen = curPos + buf.length;
                delta = afterLen - raFd.length();

                // cannot write and make the file shorter
                if (delta < 0) {
                    delta = 0;
                }
            } catch (IOException eIO) {
                return EIO;
            }

            // request cache for more space
            synchronized (cacheLock) {
                int rv = proxyCache.adjustUsage(delta);
                if (rv < 0) {
                    return rv;
                }
                proxyCache.accessFile(openedFdMap.get(fd).tmpName, 0);
            }

            // perform write operation
            try {
                raFd.write(buf);
            } catch (IOException eIO) {
                return EIO;
            }

		    // Java makes sure specified bytes are written to file exactly
            return buf.length;
		}


        /**
         * Read interface would read the file assigned for client for particular
         * fd. Multiple clients may read the same file with shared manner.
         *
         * @param fd file descriptor provided by client
         * @param buf buffer to write back to client
         * @return count of bytes read
         */
		public long read(int fd, byte[] buf) {

		    if (checkDir(fd)) {
		        return FileHandling.Errors.EISDIR;
            }

		    RandomAccessFile raFd = getFdFromMap(fd, false);
            if (raFd == null) {
                return Errors.EBADF;
            }

            int rv;
            try {
                rv = raFd.read(buf);
            } catch (IOException eIO) {
                return EIO;
            }

            synchronized (cacheLock) {
                proxyCache.accessFile(openedFdMap.get(fd).tmpName, 0);
            }

            // No data read, in Java return -1, in C return 0
            if (rv == -1) {
                return 0;
            }

            return rv;
		}


        /**
         * Lseek interface would perform file seek operation on behalf of client,
         * with LseekOption processed by our proxy.
         *
         * @param fd requested fd by client
         * @param pos position length from starting point specified by LseekOption
         * @param o can seek from start, end or current fp
         * @return result fp after seek has performed from start
         */
		public long lseek(int fd, long pos, LseekOption o) {

            if (checkDir(fd)) {
                return Errors.EISDIR;
            }

            RandomAccessFile raFd = getFdFromMap(fd, false);
            if (raFd == null) {
                return Errors.EBADF;
            }

            long target_pos;

            // reset starting point according to lseekoption
            try {
                switch (o) {
                    case FROM_START:
                        target_pos = pos;
                        break;
                    case FROM_CURRENT:
                        target_pos = raFd.getFilePointer() + pos;
                        break;
                    case FROM_END:
                        target_pos = raFd.length() + pos;
                        break;
                    default:
                        return Errors.EINVAL;
                }
            } catch (IOException eIO) {
                return EIO;
            }

            try {
                raFd.seek(target_pos);
            } catch (IOException eIO) {
                return Errors.EINVAL;
            }

            synchronized (cacheLock) {
                proxyCache.accessFile(openedFdMap.get(fd).tmpName, 0);
            }

            return target_pos;
		}


        /**
         * Unlink interface would delete the request file on the server, and
         * clean the cache if the file is in the cache also.
         * The unlink operation would not immediately delete the shared reading file
         * but reset the master pointer, so that after all clients reading that file
         * close the file, that file would automatically got deleted.
         *
         * @param path the file to unlink
         * @return status code
         */
		public int unlink(String path) {

		    String unlinkPath = Proxy.cacheDir + path;
		    File unlinkFile = new File(unlinkPath);
            int rv = 0;

		    try {
		        rv = server.unlinkFile(path);
            } catch (RemoteException eRemote) {
		        return Errors.EBUSY;
            }

            synchronized (cacheLock) {
                String curMaster = masterDict.get(unlinkPath);
                if (curMaster != null && proxyCache.getReference(curMaster) <= 0) {
                    proxyCache.deleteNode(unlinkPath, true);
                }
                masterDict.remove(unlinkPath);
            }

            return rv;

		}


        /**
         * Close interface would close the assigned file handler to client.
         * It would also execute lots of complicated work, include:
         *   - Propagate dirty data to server (write through)
         *   - Merge tmp file to master copy according to read or write state
         *   - Refresh file access again for correct LRU behavior
         *
         * @param fd file descriptor provided by client
         * @return status code
         */
        public int close(int fd) {

            int rv;

            RandomAccessFile raFd = getFdFromMap(fd, false);
            if (raFd == null) {
                return Errors.EBADF;
            }

            FileInfo fileInfo = openedFdMap.get(fd);
            String tmpName = fileInfo.tmpName;

            /* if closed file is RDWR and not directory, propagate to server */
            if (fileInfo.writePerm && !fileInfo.isDir) {
                String realName = fileInfo.rawName;
                byte[] buffer;
                long fileSize;

                try {

                    fileSize = raFd.length();
                    raFd.seek(0);

                    // normal file, one-pass write through
                    if (fileSize <= CHUNK_SIZE) {
                        buffer = new byte[(int)raFd.length()];
                        raFd.read(buffer);

                        rv = server.postFile(realName, buffer);
                        if (rv != 0) {
                            return rv;
                        }
                    }

                    // large file, use loop to post whole file
                    else {
                        long curPos = 0;
                        buffer = new byte[(int)CHUNK_SIZE];
                        raFd.read(buffer);
                        server.postLargeFile(realName, buffer, curPos, true, false);
                        curPos += CHUNK_SIZE;

                        // looping post
                        while (curPos + CHUNK_SIZE < fileSize) {
                            raFd.read(buffer);
                            server.postLargeFile(realName, buffer, curPos, false, false);
                            curPos += CHUNK_SIZE;
                        }

                        // last post
                        buffer = new byte[(int)(fileSize - curPos)];
                        raFd.read(buffer);
                        server.postLargeFile(realName, buffer, curPos, false, true);
                    }
                } catch (IOException eIO) {
                    return EIO;
                }
            }

            // close tmp file
            try {
                raFd.close();
            } catch (IOException eIO) {
                return EIO;
            }


            /* Update Cache Management Nodes */
            String masterName = masterDict.get(fileInfo.fileName);

            try {
                // delete tmp file and copy to master copy if write requested
                if (fileInfo.writePerm) {
                    synchronized (globalFileLock) {

                        synchronized (cacheLock) {
                            proxyCache.deleteNode(tmpName, false);

                            // if master opened (by readers), create another master
                            // if no master (creating new file), create another master
                            if (masterName == null ||
                                    proxyCache.getReference(masterName) > 0) {
                                masterName = fileInfo.fileName + ".tmpread." +
                                        MyHash.md52Str(MyHash.getHash(tmpName));
                                masterDict.put(fileInfo.fileName, masterName);
                            }

                            // if master can be overwritten, temporarily delete cache node for it
                            else {
                                proxyCache.deleteNode(masterName, false);
                            }
                        }

                        // replace master file by tmp file
                        Files.move(Paths.get(tmpName),
                                Paths.get(masterName),
                                REPLACE_EXISTING,
                                ATOMIC_MOVE);

                        // refresh master access history
                        synchronized (cacheLock) {
                            proxyCache.accessFile(masterName, LRUCache.EXIST_FILE);
                        }

                    }
                }

                // if only read requested, just decrease reference
                // if no reference and not master, delete tmp file
                else {
                    synchronized (cacheLock) {
                        proxyCache.changeReference(tmpName, -1);
                        proxyCache.accessFile(tmpName, LRUCache.EXIST_FILE);
                        if ((masterName == null || !masterName.equals(tmpName)) &&
                                proxyCache.getReference(tmpName) <= 0) {
                            proxyCache.deleteNode(tmpName, true);
                        }
                    }
                }
            } catch (IOException eIO) {
                return EIO;
            }

            // delete the entry in map
            openedFdMap.remove(fd);

            return 0;

        }


        /**
         * When noticed client is done, check whether any fd in the map not closed.
         * Close them all and execute corresponding action.
         */
        public void clientdone() {

            for (int key: openedFdMap.keySet()) {
                close(key);
            }
		}

        /*-- SUBROUTINES FOR OPEN PROCEDURE --*/

        /**
         * Main purpose for this function is to check various possible errors like
         * file not exist, file is directory... and return proper error numbers.
         *
         * The function would call other functions to do real work like update cache
         * or create tmp files.
         *
         * @param name fileName to open
         * @param mode Mode for random access file, "r" or "rw"
         * @param requireFileExist Whether the file specified by path should exist
         * @param requireFileNotExist Whether the file specified by path should not exist
         * @param myInfo FileInfo Class to store information
         * @return status code (C-style)
         */
		private static synchronized int openFileSafe(String name,
                                 String mode,
                                 boolean requireFileExist,
                                 boolean requireFileNotExist,
                                 FileInfo myInfo) {

            // update if write permission requested
            if (mode.equals("rw")) {
                myInfo.writePerm = true;
            }

		    int rv = updateCacheFile(name, myInfo.rawName, requireFileNotExist);
            String path = Proxy.cacheDir + name;

            // No entry in server
            if (rv == Errors.ENOENT) {
                // if CREATE_NEW || CREATE, this is not error
                if (!requireFileExist) {
                    // create tmp file, master file would be written on close
                    int rv2;
                    rv2 = openTmpFile(path, myInfo);
                    if (rv2 < 0) {
                        return rv2;
                    }
                    myInfo.writePerm = true;
                    return 0;
                } else {
                    return Errors.ENOENT;
                }
            }

            // Server side file is directory
            if (rv == Errors.EISDIR) {
                if (mode.equals("rw")) {
                    // if write permission requested, error
                    return Errors.EISDIR;
                } else {
                    // if not, store dir info only for close
                    myInfo.raFd = null;
                    myInfo.fileName = path;
                    myInfo.isDir = true;
                    return 0;
                }
            }

            // Report other failure codes
            if (rv < 0) {
                return rv;
            }

            /* This part is probably not tested */

            // try open and close the master copy, test permission
            RandomAccessFile raFd;
	        try {
                raFd = new RandomAccessFile(masterDict.get(path), mode);
                raFd.close();
            }
            // Cannot find file, cannot happen
            catch (FileNotFoundException eFileNotFound) {
	            return Errors.ENOENT;
            }
            // No permission to open file
            catch (SecurityException ePerm) {
                return Errors.EPERM;
            }

            catch (IOException eIO) {
                return EIO;
            }

            /* This part is probably not tested */


            // open tmp file, update information related to fd and return
            rv = openTmpFile(path, myInfo);
            return rv;
        }


        /**
         * Given the name of requested filename, open a tmp file and store
         * the file handler for that tmp file for client later usage.
         *
         * Write tmp file is created for each client requested for write
         * Read tmp file would always return current master copy
         *
         * @param path Pathname of master copy
         * @return Status code
         */
        private static int openTmpFile(String path, FileInfo myInfo) {

            int rv;
            File fd;
            String tmpName;
            String masterPath = masterDict.get(path);

            // if write request, simply find a new tmp file starting from 1
            // by skipping existing tmp file
            if (myInfo.writePerm) {

                String prefix = masterPath;

                // if master not exist (O_CREAT), create a tmp master filename for it
                // the name would be changed to normal format on close
                if (masterPath == null) {
                    prefix = path + ".tmpcreate";
                }

                // calculate tmp filename
                int tmpIndex = 0;
                do {
                    tmpIndex += 1;
                    fd = new File(prefix + ".tmpwrite." + String.valueOf(tmpIndex));
                } while (fd.exists());

                tmpName = prefix + ".tmpwrite." + String.valueOf(tmpIndex);

                // create the tmp file
                if ((rv = createTmpFile(masterPath, tmpName)) < 0) {
                    return rv;
                }
            }

            // read request, just give master file to user
            else {
                tmpName = masterPath;
                fd = new File(tmpName);

                // increase share reference count
                synchronized (cacheLock) {
                    proxyCache.changeReference(tmpName, 1);
                }
            }

            // open and save random access file info in myInfo struct
            try {
                myInfo.raFd = new RandomAccessFile(fd, "rw");
            } catch (FileNotFoundException eFileNotFound) {
                return Errors.ENOENT;
            }

            myInfo.tmpName = tmpName;
            myInfo.fileName = path;

            return 0;
        }


        /**
         * This function would only get called in write request. Create corresponding
         * tmp file by master name and tmp name.
         *
         * @param masterPath path of master file
         * @param tmpPath path of tmp file
         * @return status code
         */
        private static int createTmpFile(String masterPath, String tmpPath) {

            File master;
            if (masterPath != null) {
                master = new File(masterPath);
            } else {
                master = null;
            }

            File tmp = new File(tmpPath);

            try {
                // master file in cache
                if (masterPath != null) {

                    // tmp file must not exist
                    synchronized (cacheLock) {
                        // temporarily increase master file count reference,
                        // to prevent tmp file evict corresponding master file
                        proxyCache.changeReference(masterPath, 1);
                        int rv = proxyCache.accessFile(tmpPath, master.length());
                        if (rv < 0) {
                            // no space, decrease master reference
                            proxyCache.changeReference(masterPath, -1);
                            return rv;
                        }

                        // allocated space, adjust reference now
                        // keep tmp file as opened, no eviction
                        proxyCache.changeReference(tmpPath, 1);
                        proxyCache.changeReference(masterPath, -1);
                    }

                    // create real file
                    synchronized (globalFileLock) {
                        Files.copy(master.toPath(), tmp.toPath());
                    }
                }
                // master file not exist, it is CREATE NEW FILE operation
                else {
                    if (!tmp.createNewFile()) {
                        return Errors.ENOMEM;
                    }

                    synchronized (cacheLock) {
                        proxyCache.accessFile(tmpPath, 0);
                        proxyCache.changeReference(tmpPath, 1);
                    }
                }
            } catch (IOException eIO) {
                return EIO;
            }

            return 0;
        }


        /**
         * Helper function to contact server and update local cache file.
         * Operation would also update cache node if applicable.
         *
         * @param name file to update
         * @return update status
         */
        private static int updateCacheFile(String name, String realName, boolean requireFileNotExist) {

            String dir = Proxy.cacheDir + name;
            String masterCopyDir = masterDict.get(dir);

            File tryFile;
            byte[] digest = new byte[0];

            // if master version of the file exist, compute hash for query
            if (masterCopyDir != null) {
                tryFile = new File(masterCopyDir);

                if (tryFile.exists()) {
                    try {
                        digest = MyHash.getHash(masterCopyDir);
                    } catch (IOException eIO) {
                        return EIO;
                    }
                }
            }


            /* get updated version from server */
            GetFileStatus getFileStatus;
            try {
                getFileStatus = Proxy.server.getFile(realName, digest);
            } catch (RemoteException eRemote) {
                // fail to connect to server
                return EIO;
            }

            // return errno directly
            if (getFileStatus.status < 0) {
                return getFileStatus.status;
            }

            // If CREATE_NEW, then file-exist is error
            if (requireFileNotExist && getFileStatus.status != Errors.ENOENT) {
                return Errors.EEXIST;
            }

            // if cache valid, no need to update
            if (getFileStatus.status == MyHash.HASH_VALID) {
                synchronized (cacheLock) {
                    proxyCache.accessFile(masterCopyDir, 0);
                }
                return 0;
            }


            /* cached master is not valid, need to update */

            // if current master is opened or no current master, create new master
            if (masterCopyDir == null || proxyCache.getReference(masterCopyDir) > 0) {
                masterCopyDir = dir + ".tmpmaster";
            }
            tryFile = new File(masterCopyDir);

            // request space to LRU cache
            long fileSize = getFileStatus.length;
            synchronized (cacheLock) {
                int rv;
                long delta = fileSize - tryFile.length();

                // try increase space
                rv = proxyCache.adjustUsage(delta);
                if (rv < 0) {
                    // notify server error, release lock please
                    if (getFileStatus.largeFile) {
                        try {
                            Proxy.server.getLargeFile(realName, fileSize, true);
                        } catch (RemoteException eRemote) {
                            return EIO;
                        }
                    }
                    return rv;
                }

                // decrease back the space
                proxyCache.adjustUsage(-delta);
                proxyCache.deleteNode(masterCopyDir, false);
            }

            // not a large file, write in one-pass
            if (!getFileStatus.largeFile) {
                try {
                    synchronized (globalFileLock) {
                        RandomAccessFile raFd = new RandomAccessFile(tryFile, "rw");
                        raFd.setLength(0);
                        raFd.write(getFileStatus.buffer);
                        raFd.close();
                    }
                } catch (FileNotFoundException eNoFile) {
                    // impossible, no disk space
                    return Errors.ENOMEM;
                } catch (IOException eIO) {
                    return EIO;
                }
            }

            // use loop to retrieve master file chunk by chunk
            else {
                synchronized (globalFileLock) {
                    try {
                        RandomAccessFile raFd = new RandomAccessFile(tryFile, "rw");
                        raFd.setLength(0);
                        long curPos = 0;
                        byte[] ret;
                        while (curPos + CHUNK_SIZE < fileSize) {
                            ret = Proxy.server.getLargeFile(realName, curPos, false);
                            raFd.write(ret);
                            curPos += CHUNK_SIZE;
                        }
                        ret = Proxy.server.getLargeFile(realName, curPos, true);
                        raFd.write(ret);
                        raFd.close();
                    } catch (FileNotFoundException eNotFound) {
                        // impossible
                        return Errors.ENOMEM;
                    } catch (IOException eIO) {
                        return EIO;
                    }
                }
            }

            //now temp master file is created, rename it to hash version and save as master
            try {
                synchronized (cacheLock) {
                    String hashSuffix = MyHash.md52Str(MyHash.getHash(masterCopyDir));
                    String newMasterDir = dir + ".tmpread." + hashSuffix;

                    Files.move(tryFile.toPath(), Paths.get(newMasterDir));
                    masterDict.put(dir, newMasterDir);
                    proxyCache.accessFile(newMasterDir, LRUCache.EXIST_FILE);
                }
            } catch (IOException eIO) {
                return EIO;
            }

            return 0;
        }



        /*-- PRIVATE HELPER FUNCTIONS --*/

        /**
         * Check whether provided fd is valid (In our map + not dir)
         */
        private boolean checkDir(int fd) {

            if (openedFdMap.get(fd) == null) {
                return false;
            }

            return openedFdMap.get(fd).isDir;
        }

        /**
         * Get corresponding randomAccessFile handler from fd.
         * Check write permission is allowed on the fly.
         */
        private RandomAccessFile getFdFromMap(int fd, boolean checkWrite) {

            FileInfo info = openedFdMap.get(fd);
            if (info == null) {
                return null;
            }

            if (checkWrite && !info.writePerm) {
                return null;
            }

            return info.raFd;
        }

    }





	private static class FileHandlingFactory implements FileHandlingMaking {
		public FileHandling newclient() {
			return new FileHandler();
		}
	}

	public static void main(String[] args) throws IOException {

		if (args.length < 4) {
		    System.out.println("Usage [ip] [port] [cache dir] [cache limit]");
		    return;
        }

        ServerIntf myServer;
        try {
            Registry reg = LocateRegistry.getRegistry(args[0], Integer.parseInt(args[1]));
            myServer = (ServerIntf) reg.lookup("CacheServer");
        } catch (Exception eGeneral) {
            System.out.println("Client Initializing Error: " + eGeneral.getMessage());
            eGeneral.printStackTrace();
            return;
        }

        server = myServer;
        cacheDir = args[2] + "/";
        proxyCache = new LRUCache(Long.parseLong(args[3]));

		(new RPCreceiver(new FileHandlingFactory())).run();
	}
}

